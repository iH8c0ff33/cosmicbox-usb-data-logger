extern crate chrono;
extern crate cosmicbox;
extern crate futures;
extern crate hidapi;
extern crate hyper;
extern crate hyper_tls;
extern crate tokio_core;

use chrono::{DateTime, Utc};
use cosmicbox::{CosmicBox, GenericCosmicBox, TriggerOptions};
use futures::Future;
use hyper::header::{ContentLength, ContentType};
use hyper::{Client, Method, Request, StatusCode};
use hyper_tls::HttpsConnector;
use std::env;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;
use tokio_core::reactor::Core;

fn post_timestamp(
    url: &String,
    core: &mut tokio_core::reactor::Core,
    client: &hyper::Client<hyper_tls::HttpsConnector<hyper::client::HttpConnector>>,
    timestamp: DateTime<Utc>,
) {
    let json = format!("{{\"time\":\"{}\"}}", timestamp.format("%FT%T%.9fZ"));

    let uri = url.parse().unwrap();
    let mut request = Request::new(Method::Post, uri);
    request.headers_mut().set(ContentType::json());
    request.headers_mut().set(ContentLength(json.len() as u64));
    request.set_body(json);

    core.run(client.request(request).map(|res| {
        if let StatusCode::Ok = res.status() {
            println!("INFO: POST done @{}", timestamp);
        } else {
            println!("ERR: POST failed @{}", timestamp);
        }
    }))
    .unwrap_or_else(|err| println!("ERR: Couldn't post event: {}", err));
}

fn post_timestamps(rx: mpsc::Receiver<DateTime<Utc>>, url: String) {
    let mut core = Core::new().expect("ERR: Couldn't instantiate an event loop!");
    let handle = core.handle();
    let client = Client::configure()
        .connector(HttpsConnector::new(4, &handle).expect("ERR: Failed to initialize TLS"))
        .build(&handle);

    while let Ok(timestamp) = rx.recv() {
        post_timestamp(&url, &mut core, &client, timestamp);
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let url = String::from(&args[1][..]);

    // HID Setup
    println!("INFO: started, trying to acquire HID access");
    let hid = hidapi::HidApi::new().expect("ERR: Failed to get HID access!");
    println!("INFO: Connection to CosmicBox...");
    let cosmicbox = CosmicBox::connect(hid);
    println!("INFO: Connected!");

    println!("INFO: Starting and resetting counters");
    cosmicbox.reset().expect("couldn't reset cosmicbox");
    cosmicbox
        .set_trigger(&TriggerOptions {
            top: true,
            bottom: true,
            ext: false,
        })
        .expect("couldn't set trigger");
    thread::sleep(Duration::from_millis(16));

    let (tx, rx) = mpsc::channel();
    thread::spawn(|| post_timestamps(rx, url));

    let mut last: u16 = 0;

    loop {
        match cosmicbox.get_counters() {
            Ok(counters) => {
                if counters.coinc != last {
                    let timestamp = Utc::now();
                    println!("CB#1 event #{} @{}", counters.coinc, timestamp);
                    tx.send(timestamp)
                        .expect("ERR: Couldn't send timestamp to poster thread!");
                    last = counters.coinc;
                }
            }
            Err(_) => continue,
        }
    }
}
