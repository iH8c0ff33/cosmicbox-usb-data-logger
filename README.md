# Cosmicbox V3

## Protocol reverse engineering

### Protocol packet format

#### TX command packet format

|value|length|
|-|-:|
|MajorCmd|1 Byte|
|MinorCmd|1 Byte|
|Data LSB|1 Byte|
|Data MSB|1 Byte|
|DataHID[0..3]|4 Bytes|
|DataEXT[0..7]?|8 Bytes|

#### RX packet format

|value|length|
|-|-:|
|Read Data|8-16 Bytes|


> NOTE: All signals are active on **LOW**

The cosmicbox controller has two ports:

- Port 0 is used for loading counters data
- Port 1 is used for controlling the cosmicbox and the address for counter data

Counter addresses (all of them are negated) are the following:

- `0b000`: low byte TOP (gets negated to `0b111`)
- `0b001`: high byte TOP
- `0b010`: low byte BOTTOM
- `0b011`: high byte BOTTOM
- `0b100`: low byte EXT
- `0b101`: high byte EXT
- `0b110`: low byte COINCIDENCE
- `0b111`: high byte COINCIDENCE

### Setting counter address

In order to set the counter address you can do the following:

```rust
device.send_feature_report(&[101, 12, !i & 0b111, 0b111])
```

- `101` is _MajorCmd_ for the 8 byte write command
- `12` is _MinorCmd_ for setting **port 1** pins
- `0b111` being negates (as specified above) is the counter address
> NOTE: Data LSB is for _setting_ port pins
- `0b111` is for resetting the 3 least significant bits
> NOTE: Data MSB is for _resetting_ port pins (ie: getting them **LOW**)

### Setting up acquisition

#### Port 1 usage

|index|function|
|-|-:|
|P1_0|CounterADDR[0]|
|P1_1|CounterADDR[1]|
|P1_2|CounterADDR[2]|
|P1_3|START/STOP|
|P1_4|Reset|
|P1_5|EXT Coinc.|
|P1_6|BOTTOM Coinc.|
|P1_7|TOP Coinc.|

> NOTE: Start is **LOW** (0) while Stop is **HIGH** (1)
> NOTE: Reset is triggeres on falling edge (ie: **HIGH** (1) to **LOW** (0))

Example (turn off EXT Coinc.):

```rust
device.send_feature_report(&[101, 12, 1 << 5, 0])
```

- `1<<5` is the port 1 address for _EXT Coinc._
> NOTE: `1<<5` is being *SET* and not *RESET*, since it needs to be *HIGH* (1) which is the non-active state

### Reading data

The first step for reading data is to set the read command

```rust
let mut data = [100; 3];
device.get_feature_report(&mut data)
```

- `data[0]` is the MajorCMD, and 100 is read 8 Bytes
- `data[1]` is the MinorCMD, and 100 is read port 0 and 1

> NOTE: `data` is 3 Bytes long for storing the received data
